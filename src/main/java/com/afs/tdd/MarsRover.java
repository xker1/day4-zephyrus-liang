package com.afs.tdd;

import java.util.ArrayList;
import java.util.Map;

public class MarsRover {
    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)){
            move();
        }else if (command.equals(Command.Left)){
            turnLeft();
        }else if (command.equals(Command.Right)){
            turnRight();
        }
    }

    private final Map<Direction, Direction> turnRightMap = Map.of(
            Direction.North, Direction.East,
            Direction.East, Direction.South,
            Direction.South, Direction.West,
            Direction.West, Direction.North
    );

    private final Map<Direction, Direction> turnLeftMap = Map.of(
            Direction.North, Direction.West,
            Direction.East, Direction.North,
            Direction.South, Direction.East,
            Direction.West, Direction.South
    );

    private void turnRight() {
        Direction direction = location.getDirection();
        location.setDirection(turnRightMap.get(direction));
    }

    private void turnLeft() {
        Direction direction = location.getDirection();
        location.setDirection(turnLeftMap.get(direction));
    }

    private void move() {
        if (location.getDirection().equals(Direction.North))
            location.setCoordinateY(location.getCoordinateY() + 1);
        if (location.getDirection().equals(Direction.South))
            location.setCoordinateY(location.getCoordinateY() - 1);
        if (location.getDirection().equals(Direction.East))
            location.setCoordinateX(location.getCoordinateX() + 1);
        if (location.getDirection().equals(Direction.West))
            location.setCoordinateX(location.getCoordinateX() - 1);
    }

    public Location getLocation() {
        return this.location;
    }

    public void executeBatchCommands(ArrayList<Command> commands) {
        commands.forEach(this::executeCommand);
    }
}
