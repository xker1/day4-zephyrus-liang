O:

    1. Unit Test

        (1) Learn the components of Unit Test: Test Description, GIVEN/Prepare, WHEN/execute, THEN/Verify
        (2) Learn how to write a qualified three-stage testing method through some examples

    2. Test Driven Development
        (1) Learn the workflow of TDD and how to perform it: Write a test that fails; Make the code work; Eliminate redundancy
        (2) Experience the benefits of TDD through a simple example: Simple design; Test as documentation; Quick feedback

R: I feel very fulfilled.

I: The Unit Test and TDD that I learned today are easy to master and practice. During the coding process, I felt the charm of Test Driven Development, which can effectively exercise programming thinking.

D:I will search for more cases of TDD on the internet for learning and apply it to daily development.